<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('welcome', function () {
    return view('welcome');
});

Route::get('userType', function () {
    return view('userType');
});

Route::get('newStudent', function () {
    return view('newStudent');
});

Route::get('onboarding1', function () {
    return view('onboarding1');
});

Route::get('onboarding2', function () {
    return view('onboarding2');
});

Route::get('onboarding3', function () {
    return view('onboarding3');
});

Route::get('onboarding4', function () {
    return view('onboarding4');
});

Route::get('onboarding5', function () {
    return view('onboarding5');
});

Route::get('studentHome', function () {
    return view('student_home');
});

Route::get('university', function () {
    return view('university_content');
});

Route::get('scholarship', function () {
    return view('scholarships');
});