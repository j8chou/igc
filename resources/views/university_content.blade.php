<!DOCTYPE html>
<html>
    <head>
        <title>IGC</title>

        {!! Html::style('styles/main.css') !!}
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style>
          textarea {
            height: auto;
          }
        </style>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

</head>
    <body>
        <div class="navbar-fixed">
          <nav>
            <div class="nav-wrapper">
              <a href="studentHome" class="brand-logo">&nbspIGC</a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="welcome" onclick="">Logout</a></li>
              </ul>
            </div>
          </nav>
        </div>

        <div class="container">
          <div class="row">
            <div class="col s12">
                <div class="col s12 m6">
                  <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                      <span class="card-title">Frequently Asked Questions</span>
                      <p>
                          How early should I start applying?<br>
                          As early as a year prior to entering college.<br><br>

                          How long does it take to hear back?<br>
                          Applications are due in November, and results are released in the spring.
                      </p>
                    </div>
                    <div class="card-action">
                      <a href="#">Learn More</a>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6">
                  <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                      <span class="card-title">Applying To Universities in the U.S.</span>
                      <p>
                          Here are the tests you need to take:<br><br>
                          1. Admission Test: SAT or ACT<br>
                          2. English Proficiency Assessments: TOEFL test<br><br>
                          All applications are online.<br>
                      </p>
                    </div>
                    <div class="card-action">
                      <a href="#">Learn More</a>
                    </div>
                  </div>
                </div>
                

                <div class="col s12 m6">
                  <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                      <img class="activator" src="background/visa.jpg">
                    </div>
                    <div class="card-content">
                      <span class="card-title activator grey-text text-darken-4">Applying For Student Visa<i class="material-icons right">more_vert</i></span>
                      <p><a href="#">Learn More</a></p>
                    </div>
                    <div class="card-reveal">
                      <span class="card-title grey-text text-darken-4">Applying For Student Visa<i class="material-icons right">close</i></span>
                      <p>
                        If you are a student, there are different types of student Visa you can apply. <br><br>
                        You can visit this website in <a href="http://www.internationalstudent.com/immigration/">English</a> or <a href="https://translate.google.com/translate?sl=en&tl=ne&js=y&prev=_t&hl=en&ie=UTF-8&u=http://www.internationalstudent.com/immigration/&edit-text=">Nepali</a> for more information. <br><br>
                        There are also other types of Visa for businesses, professionals, etc. You can read more about them here: <a href="http://travel.state.gov/content/visas/en/study-exchange.html">English</a>, <a href="https://translate.google.com/translate?sl=en&tl=ne&js=y&prev=_t&hl=en&ie=UTF-8&u=http://travel.state.gov/content/visas/en/study-exchange.html&edit-text=">Nepali</a>.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6">
                  <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                      <span class="card-title">Scholarships for Studying In U.S.</span>
                      <p>
                          Everyone can afford their studying abroad...
                      </p>
                    </div>
                    <div class="card-action">
                      <a href="scholarship">Learn More</a>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6">
                  <div class="card teal darken-3">
                    <div class="card-content white-text">
                      <span class="card-title">Have A Question?</span>
                    </div>
                    <div class="card-action">
                      <a class="modal-trigger" href="#modal1">Ask Here</a>
                    </div>
                  </div>
                </div>
                <div class="col s12">
                  <a href="studentHome"class="waves-effect waves-light btn">Back</a>
                </div>
            </div>  
          </div>
        </div>

        <div id="modal1" class="modal modal-fixed-footer">
          <div class="modal-content">
            <h4>Ask A Question</h4>
              {!! Form::textarea('textarea1',null, array('rows'=>13)) !!}
          </div>
          <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Submit</a>     
          </div>
        </div>
        
        <script>     
            $(document).ready(function(){
              // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
              $('.modal-trigger').leanModal();
            });
        </script>
        {!! Html::script('scripts/vendor.js') !!}
        {!! Html::script('scripts/main.js') !!}
    </body>
</html>
