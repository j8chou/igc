<!DOCTYPE html>
<html>
    <head>
        <title>IGC</title>

        {!! Html::style('styles/main.css') !!}
    
    <style>
        body{
            overflow-y: hidden;
        }

        .transparent {
            background-color: #bdbdbd !important;
            color: black;
            border-style: solid;
            border-color: black;
            border-width: 2px;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
            filter: alpha(opacity=50);
            opacity: 0.5;
        }
    </style>
    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          <a href="/" class="brand-logo">&nbspIGC</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="welcome">Sign In</a></li>
            <li><a href="userType" onclick="">Register</a></li>
          </ul>
        </div>
      </nav>
    </div>

    <div>
        <div id="landing-container" class="center-align background" style="margin-top: -25px !important;">
            <div>
              <div class="landing-slide" style="background: url(background/landingBackground.jpg); height: 100vh;">
              </div>
            </div>
        </div>

        <div class="container" style="margin-top: 46vh;">
            <div class="row">
                <div class="col s12 m4 offset-m1">
                    <a href="newStudent" class="btn btn-large waves-effect transparent"><strong>I AM A STUDENT</strong></a>
                </div>
                <div class="col s12 m4 offset-m3">
                    <a href="" class="btn btn-large waves-effect transparent"><strong>I AM A TEACHER</strong></a>
                </div>
            </div>
        </div>
    </div>

    {!! Html::script('scripts/vendor.js') !!}
    {!! Html::script('scripts/main.js') !!}
    
    </body>
</html>
