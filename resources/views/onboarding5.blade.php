<!DOCTYPE html>
<html>
    <head>
        <title>IGC</title>

        {!! Html::style('styles/main.css') !!}

    <style>
        .card-panel{
            height: 130px;
            width: 160px;
        }

        .white-text {
          font-size: 1.5em;
        }
    </style>
    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          <a href="onboarding1" class="brand-logo">&nbspIGC</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="welcome" onclick="">Logout</a></li>
          </ul>
        </div>
      </nav>
    </div>

    <div>
        <div id="landing-container" class="center-align background" style="margin-top: -40px !important;">
            <div>
              <div class="landing-slide" style="background: url(background/onboarding5.jpg); height: 100vh;">
              </div>
            </div>
        </div>

        <div class="container" style="margin-top: 30vh;">
            <div class="row">
                 <div class="col s12">
                    <div class="row">
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal ">
                          <input type="checkbox" class="filled-in" id="art"/>
                          <label for="art"></label>
                          <div class="center-align">
                          <span class="white-text">
                            ART
                          </span>
                          </div>
                        </div>
                      </div>
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal">
                          <input type="checkbox" class="filled-in" id="crafts"/>
                          <label for="crafts"></label>
                          <div class="center-align">
                          <span class="white-text">
                            CRAFTS
                          </span>
                          </div>
                        </div>
                      </div>
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal">
                          <input type="checkbox" class="filled-in" id="teaching"/>
                          <label for="teaching"></label>
                          <div class="center-align">
                          <span class="white-text">
                            TEACHING
                          </span>
                          </div>
                        </div>
                      </div>
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal">
                          <input type="checkbox" class="filled-in" id="tech"/>
                          <label for="tech"></label>
                          <div class="center-align">
                          <span class="white-text">
                            TECH
                          </span>
                          </div>
                        </div>
                      </div>
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal">
                          <input type="checkbox" class="filled-in" id="business"/>
                          <label for="business"></label>
                          <div class="center-align">
                          <span class="white-text">
                            BUSINESS
                          </span>
                          </div>
                        </div>
                      </div>
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal">
                          <input type="checkbox" class="filled-in" id="politics"/>
                          <label for="politics"></label>
                          <div class="center-align">
                          <span class="white-text">
                            POLITICS
                          </span>
                          </div>
                        </div>
                      </div>
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal">
                          <input type="checkbox" class="filled-in" id="biology"/>
                          <label for="biology"></label>
                          <div class="center-align">
                          <span class="white-text">
                            BIOLOGY
                          </span>
                          </div>
                        </div>
                      </div>
                      <div class="col s12 m3">
                        <div class="card-panel waves-effect teal">
                          <input type="checkbox" class="filled-in" id="chemistry"/>
                          <label for="chemistry"></label>
                          <div class="center-align">
                          <span class="white-text">
                            CHEMISTRY
                          </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col s12 m2 offset-m4">
                   <a href="onboarding4" class="btn waves-effect waves-light grey lighten-1 black-text">Back</a>
                </div>
                <div class="col s12 m2">
                   <a href="studentHome" class="btn waves-effect waves-light">Next</a>
                </div>
              </div>
        </div>
    </div>

    {!! Html::script('scripts/vendor.js') !!}
    {!! Html::script('scripts/main.js') !!}

    </body>
</html>
