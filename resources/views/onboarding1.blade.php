<!DOCTYPE html>
<html>
    <head>
        <title>IGC</title>

        {!! Html::style('styles/main.css') !!}
        {!! Html::style('styles/nouislider.css') !!}

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> 

<script>
document.addEventListener("DOMContentLoaded", function(event) { 
  //do workt>
var element = document.getElementById("slider");
noUiSlider.create(element, {
    start: 15,
    animate: true,
    step: 1,
    connect: 'lower',
    tooltips:true,
    range: {
        'min': 10,
        'max': 35
    },
    format: {
      to: function ( value ) {
        return parseInt(value);
      },
      from: function ( value ) {
        return value.replace('.', '');
      }
    }

});
var label = document.getElementById("sliderLabel");

slider.noUiSlider.on('slide',function(){
    label.value=slider.noUiSlider.get()+ " minutes";
}); 

});


 function changed() {
     var value = document.getElementById("sliderLabel").value
     var element = document.getElementById("slider");
     element.noUiSlider.set(value);
 }
 </script>

    <style>
        body{
            overflow-y: hidden;
        }
    </style>
    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          <a href="onboarding1" class="brand-logo">&nbspIGC</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="welcome" onclick="">Logout</a></li>
          </ul>
        </div>
      </nav>
    </div>

    <div>
        <div id="landing-container" class="center-align background" style="margin-top: -40px !important;">
            <div>
              <div class="landing-slide" style="background: url(background/onboarding1.jpg); height: 100vh;">
              </div>
            </div>
        </div>

        <div class="container" style="margin-top: 40vh;">
            <div class="row">
                 <div class="col s12">
                    <div id="slider">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m2">
                    <div class="card-panel waves-effect teal">
                      <span class="white-text">&nbsp10&nbsp
                      </span>
                    </div>
                </div>
                <div class="col s12 m1 offset-m9">
                    <div class="card-panel waves-effect teal">
                      <span class="white-text">35+
                      </span>
                    </div>
                </div>
            </div>
            
            <div class="center-align">
                 <a href="onboarding2" class="btn waves-effect waves-light">Next</a>
            </div>
            
        </div>
    </div>

    {!! Html::script('scripts/vendor.js') !!}
    {!! Html::script('scripts/main.js') !!}
    {!! Html::script('scripts/nouislider.min.js') !!}

    </body>
</html>
