<!DOCTYPE html>
<html>
    <head>
        <title>IGC</title>

        {!! Html::style('styles/main.css') !!}

    <style>
        body{
            overflow-y: hidden;
        }

        .card {
          overflow: visible !important;
        }
    </style>
    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          <a href="onboarding1" class="brand-logo">&nbspIGC</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="welcome" onclick="">Logout</a></li>
          </ul>
        </div>
      </nav>
    </div>

    <div>
        <div id="landing-container" class="center-align background" style="margin-top: -40px !important;">
            <div>
              <div class="landing-slide" style="background: url(background/onboarding3.jpg); height: 100vh;">
              </div>
            </div>
        </div>

        <div class="container" style="margin-top: 35vh;">
            <div class="row">
                 <div class="col s12">
                    <div class="card">
                            <div class="card-content">
                                
                                <div class="input-field">

                                    {!! Form::select('goodat',array('' => '',
                                                                    'art'=>'Art',
                                                                    'history' => 'History',
                                                                    'language' => 'Language Arts',
                                                                    'math'=>'Math',
                                                                    'science'=>'Science',
                                                                    'sports'=>'Sports')) !!}

                                    <label for="goodat">Choose a subject you are good at!</label>
                                </div>

                            </div>
                    </div>
              </div>
              <div class="row">
                <div class="col s12 m2 offset-m4">
                   <a href="onboarding2" class="btn waves-effect waves-light grey lighten-1 black-text">Back</a>
                </div>
                <div class="col s12 m2">
                   <a href="onboarding4" class="btn waves-effect waves-light">Next</a>
                </div>
              </div>
        </div>
    </div>

    {!! Html::script('scripts/vendor.js') !!}
    {!! Html::script('scripts/main.js') !!}

    </body>
</html>
