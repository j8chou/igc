<!DOCTYPE html>
<html>
    <head>
        <title>IGC</title>

        {!! Html::style('styles/main.css') !!}
    
    <style>
        body{
            overflow-y: hidden;
        }
    </style>
    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          <a href="/" class="brand-logo">&nbspIGC</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="welcome">Sign In</a></li>
            <li><a href="userType" onclick="">Register</a></li>
          </ul>
        </div>
      </nav>
    </div>

    <div>
        <div id="landing-container" class="center-align background" style="margin-top: -25px !important;">
            <div>
              <div class="landing-slide" style="background: url(background/landingBackground.jpg); height: 100vh;">
              </div>
            </div>
        </div>

        <div class="container" style="margin-top: 34vh;">
            <div class="row">
                <div class="col s12 m4 offset-m4">
                    <div class="card">
                        <form action="onboarding1">
                            <div class="card-content">
                                <div class="center-align">
                                    <strong>NEW USER</strong> <br>
                                    Set a Username and Password!
                                </div>
                                <div class="input-field">
                                    <input type="text" name="username" id="username">
                                    <label for="username">Username</label>
                                </div>
                                <div class="input-field">
                                    <input type="password" name="password" id="password">
                                    <label for="password">Password</label>
                                </div>
                            </div>
                            <div class="card-action center-align">
                                 <button class="btn waves-effect waves-light">Sign In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Html::script('scripts/vendor.js') !!}
    {!! Html::script('scripts/main.js') !!}
    
    </body>
</html>
