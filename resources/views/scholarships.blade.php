<!DOCTYPE html>
<html>
    <head>
        <title>IGC</title>

        {!! Html::style('styles/main.css') !!}
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
    <body>
        <div class="navbar-fixed">
          <nav>
            <div class="nav-wrapper">
              <a href="studentHome" class="brand-logo">&nbspIGC</a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="welcome" onclick="">Logout</a></li>
              </ul>
            </div>
          </nav>
        </div>

        <div class="container">
          <div class="row">
            <div class="col s12">
                
              <div class="row">
                <div class="col s12 m12">
                  <h4>Scholarships for Studying in the U.S.</h4>
                  <div class="card-panel">
                    
                    Everyone can afford their studying abroad by paying their studying abroad expenses with scholarships. If you are a student in financial need, consider applying for any scholarships listed below.
                    <br><br>
                    Scholarship applications will usually require your background information and one to two essays in English. Talk to your teachers and ask them to help your proofreading your essays. 
                    <br><br>
                    Here is a useful guide for international students to get U.S. Scholarships: 
                    <a href="http://www.usnews.com/education/blogs/the-scholarship-coach/2012/03/22/an-international-students-guide-to-us-scholarships">English</a>, <a href="https://translate.google.com/translate?sl=en&tl=ne&js=y&prev=_t&hl=en&ie=UTF-8&u=http://www.usnews.com/education/blogs/the-scholarship-coach/2012/03/22/an-international-students-guide-to-us-scholarships&edit-text=">Nepali</a>.
                    <br><br>
                    Here are some scholarships that Nepali students have previously received:
                    <br><br>
                    <a href="http://usefnepal.org/grant-pages-13.html">Fulbright Foreign Student Program</a>
                    <br><br>
                    <a href="http://usefnepal.org/grant-pages-15.html">Hubert H. Humphrey Fellowship Program</a>
                    <br><br>
                    <a href="http://usefnepal.org/grant-pages-16.html">Global Undergraduate Exchange Program</a>
                    <div class="right-align">
                      <a class="btn-floating btn-large red">
                        <i class="large material-icons">mode_edit</i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <a href="{!! URL::previous() !!}"class="waves-effect waves-light btn">Back</a>
            </div>
          </div>
        </div>
                

        {!! Html::script('scripts/vendor.js') !!}
        {!! Html::script('scripts/main.js') !!}
    
    </body>
</html>
